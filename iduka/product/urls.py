from django.urls import path

from .views import productView

urlpatterns = [
    path('', productView.index, name='index'),
]
