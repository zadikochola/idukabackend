from django.db import models


class Product(models.Model):
    product_id = models.IntegerField()
    product_name = models.CharField(max_length=50)
    product_type = models.CharField(max_length=10)
    product_unit_cost = models.FloatField(max_length=10)
    product_retail_price = models.FloatField(max_length=10)
    product_manufacture = models.CharField(max_length=70)

    def __str__(self):
        return self.product_name
